import React from 'react';
import PropTypes from 'prop-types';

export const A = ({ values }) => {
  console.log(values)
return <a href={values.href} rel="noopener noreferrer" target="_blank">{values.label}</a>}
;

A.propTypes = {
  values: PropTypes.shape({
    text: PropTypes.string,
  }).isRequired,
};
