import { createSlice } from '@reduxjs/toolkit'

export const initialState = {
  currentlyEdited: null,
  items: [
  ],
};

export const componentsSlice = createSlice({
  name: 'components',
  initialState,
  reducers: {
    addComponent: (state, action) => {
      action.payload.values = {};
      state.items.push(action.payload);
      state.currentlyEdited = action.payload;
    },
    updateComponent: (state, action) => {

      state.currentlyEdited = null;
      // sacar el index de un array
      const itemId = state.items.findIndex((item)=> item.id === action.payload.id);

      if (itemId !== -1) {
        state.items[itemId].values= action.payload.data.values;
      } else {
        console.debug('El componente no se encontró');
      }
    },
    removeComponent: (state, action) => {
      const newArr = state.items.filter(item => item.id !== action.payload.id);
      state.items = newArr.slice();
      state.currentlyEdited = null;
    },
    setEditedComponent: (state, action) => {
      state.currentlyEdited = action.payload.component;
    },
  },
})

export const componentsActions = componentsSlice.actions
export const componentsReducer = componentsSlice.reducer
