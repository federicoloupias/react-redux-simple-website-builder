# React Redux Simple Website Builder

### About the app

On the left-hand side, the *Component Preview* is being displayed, it includes the current content of the page. On the right-hand side, using the *Component Picker*, the user can choose a new component to be added (headers, links, text paragraphs, images, etc). When a component is chosen, appropriate form is being displayed (*Edited Component*), for the user to enter relevant data, e.g. when user clicks to add a *Link*, *Link Form* is displayed and the user should set link *label* and *href*. Moreover:
- while editing, the user can *apply*, so that the changes are submitted, or *remove*, if the item should be removed.
- when the user chooses to add a certain component type (e.g. a link, whatever) and the appropriate form gets opened, all other components become locked (disabled). Only when the form gets closed (either element is applied or removed) then the lock is removed (user can choose to add a new component again)

# Setup

1. `npm install` – install dependencies
2. `npm test` – run all tests (this will be used to evaluate your solutions)
3. `npm start` – run the project locally